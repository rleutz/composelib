package br.com.uware.composelib

import androidx.compose.runtime.Composable
import br.com.uware.composelib.presentation.theme.AppTheme

@Composable
internal fun App() = AppTheme {

}

